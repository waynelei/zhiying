1. start uia tool by with bash script from your repo directory
    ./uia-tool.sh

2. a gui application will start, click Launch after a device id is detected
    this will launch Movies app on your iOS device

3. click Log Element Tree to log current view, you will see printout in left text area

4. interact with app by typeing javascript in right text area, and clicking Send
    sample javascript: window.elements()[1].elements()[0].tap();

target, app, window are pre-defined javascript objects in this tool.

please refer to https://developer.apple.com/library/ios/documentation/DeveloperTools/Reference/UIAutomationRef/ for more detail.
